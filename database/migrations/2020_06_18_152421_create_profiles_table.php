<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('picture')->nullable();
            $table->string('gender');
            $table->string('blood_group');
            $table->string('mobile_no');
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('department_short_code');
            $table->unsignedBigInteger('session_id');
            $table->unsignedBigInteger('batch_id');
            $table->string('department_batch');
            $table->string('registration_no')->nullable();
            $table->string('occupation');
            $table->string('current_organization');
            $table->string('designation')->nullable();
            $table->string('present_address_line_1');
            $table->string('present_address_line_2')->nullable();
            $table->unsignedBigInteger('present_country_id');
            $table->unsignedBigInteger('present_state_id');
            $table->unsignedBigInteger('present_city_id');
            $table->string('permanent_address_line_1');
            $table->string('permanent_address_line_2')->nullable();
            $table->unsignedBigInteger('permanent_country_id');
            $table->unsignedBigInteger('permanent_state_id');
            $table->unsignedBigInteger('permanent_city_id');
            $table->string('linkedIn_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('whatsApp_no')->nullable();
            $table->string('viber_no')->nullable();
            $table->string('preferred_communication_channel');

            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users');
            $table->foreign('session_id')->references('id')
                ->on('sessions');
            $table->foreign('batch_id')->references('id')
                ->on('batches');
            $table->foreign('department_id')->references('id')
                ->on('departments');
            $table->foreign('department_short_code')->references('id')
                ->on('departments');
            $table->foreign('present_country_id')->references('id')
                ->on('countries');
            $table->foreign('present_state_id')->references('id')
                ->on('states');
            $table->foreign('present_city_id')->references('id')
                ->on('cities');
            $table->foreign('permanent_country_id')->references('id')
                ->on('countries');
            $table->foreign('permanent_state_id')->references('id')
                ->on('states');
            $table->foreign('permanent_city_id')->references('id')
                ->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
