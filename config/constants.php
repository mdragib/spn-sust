<?php
/**
 * Created by PhpStorm.
 * User: Shovo
 * Date: 21-Jun-20
 * Time: 8:28 PM
 */


return [
    'occupation' => [
        'Banker' => 'Banker',
        'Teacher' => 'Teacher',
        'Software Engineer' => 'Software Engineer',
        'Entrepreneur' => 'Entrepreneur',
    ],
    'department_batch' => [
        '1st' => '1st',
        '2nd' => '2nd',
        '3rd' => '3rd',
        '4th' => '4th',
        '5th' => '5th',
        '6th' => '6th',
        '7th' => '7th',
        '8th' => '8th',
        '9th' => '9th',
        '10th' => '10th',
        '11th' => '11th',
        '12th' => '12th',
        '13th' => '13th',
        '14th' => '14th',
        '15th' => '15th',
        '16th' => '16th',
        '17th' => '17th',
        '18th' => '18th',
        '19th' => '19th',
        '20th' => '20th',
        '21st' => '21st',
        '22nd' => '22nd',
        '23rd' => '23rd',
        '24th' => '24th',
        '25th' => '25th',
        '26th' => '26th',
        '27th' => '27th',
        '28th' => '28th',
        '29th' => '29th',
        '30th' => '30th',
        '31st' => '31st',
    ],
    'blood_group' => [
        'A+' => 'A+',
        'A-' => 'A-',
        'B+' => 'B+',
        'B-' => 'B-',
        'O+' => 'O+',
        'O-' => 'O-',
        'AB+' => 'AB+',
        'AB-' => 'AB-',
    ],
    'gender' => [
        'Male' => 'Male',
        'Female' => 'Female',
        'Other' => 'Other'
    ],
    'preferred_communication_channel' => [
        'Email' => 'Email',
        'Phone' => 'Phone',
        'LinkedIn' => 'LinkedIn',
        'Facebook' => 'Facebook',
        'Twitter' => 'Twitter',
        'WhatsApp' => 'WhatsApp',
        'Viber' => 'Viber',
    ]
];