<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['session', 'batch_number'];

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}
