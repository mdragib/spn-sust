<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];

    public function states()
    {
        return $this->hasMany(State::class);
    }
    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }

}
