<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
   protected $fillable = ['session_id', 'department_id', 'batch_number'];

   public function session()
   {
       return $this->belongsTo(Session::class);
   }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}
