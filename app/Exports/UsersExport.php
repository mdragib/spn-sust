<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromCollection, WithMapping, WithHeadings
{

    private $data;

    /**
     * UsersExport constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->user->name,
            $row->user->email,
            $this->getStatus($row->user->status),
            $row->user->role,
            $row->gender,
            $row->blood_group,
            $row->mobile_no,
            $row->department->name,
            $row->session->session,
            $row->batch->batch_number,
            $row->department->department_short_code,
            $row->department_batch,
            $row->registration_no,
            $row->occupationModel->name,
            $row->current_organization,
            $row->designation,
            $row->present_address_line_1,
            $row->present_address_line_2,
            $row->country->name,
            $row->state->name,
            $row->city->name,
            $row->permanent_address_line_1,
            $row->permanent_address_line_2,
            $row->permanentCountry->name,
            $row->permanentState->name,
            $row->permanentCity->name,
            $row->linkedIn_link,
            $row->facebook_link,
            $row->twitter_link,
            $row->whatsApp_no,
            $row->viber_no,
            $row->mobile_no_privacy,
            $row->user->email_verified_at,
            $row->user->created_at,
        ];
    }

    private function getStatus($status)
    {
        if ($status == 0) {
            return "Pending";
        } elseif ($status == 1) {
            return "Active";
        } else {
            return "Inactive";
        }
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Name',
            'Email',
            'Status',
            "User role",
            'Gender',
            'Blood group',
            'Mobile no',
            'Department',
            'Session',
            'Batch number',
            'Department short code',
            'Department batch',
            'Registration no',
            'Occupation',
            'Current organization',
            'Designation',
            'Present address line 1',
            'Present address line 2',
            'Present country',
            'Present state',
            'Present city',
            'Permanent address line 1',
            'Permanent address line 2',
            'permanent Country',
            'Permanent State',
            'Permanent City',
            'LinkedIn link',
            'Facebook link',
            'Twitter link',
            'WhatsApp no',
            'Viber no',
            'Mobile no privacy',
            'Email Verified At',
            'Created at',
        ];
    }
}
