<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataExportLog extends Model
{
    protected $fillable = ['export_by', 'desc'];
    protected $table = "data_export_log";
}
