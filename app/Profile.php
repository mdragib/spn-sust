<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['user_id', 'picture', 'gender', 'blood_group', 'mobile_no', 'mobile_no_privacy', 'department_id', 'department_short_code',
        'session_id', 'batch_id', 'department_batch', 'registration_no', 'occupation', 'occupation_id', 'current_organization', 'designation',
        'present_address_line_1', 'present_address_line_2', 'present_country_id', 'present_state_id', 'present_city_id',
        'permanent_address_line_1', 'permanent_address_line_2', 'permanent_country_id', 'permanent_state_id', 'permanent_city_id',
        'linkedIn_link', 'facebook_link', 'twitter_link', 'whatsApp_no', 'viber_no', 'preferred_communication_channel'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    public function batch()
    {
        return $this->belongsTo(Session::class, 'batch_id', 'id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'present_country_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'present_state_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'present_city_id');
    }

    public function permanentCountry()
    {
        return $this->hasOne(Country::class, 'id', 'permanent_country_id');
    }

    public function permanentState()
    {
        return $this->hasOne(State::class, 'id', 'permanent_state_id');
    }

    public function permanentCity()
    {
        return $this->hasOne(City::class, 'id', 'permanent_city_id');
    }

    public function occupationModel()
    {
        return $this->hasOne(Occupation::class, 'id', 'occupation_id');
    }
}
