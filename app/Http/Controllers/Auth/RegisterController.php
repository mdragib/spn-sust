<?php

namespace App\Http\Controllers\Auth;

use App\Batch;
use App\City;
use App\Country;
use App\Department;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Occupation;
use App\Profile;
use App\Session;
use App\State;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = "/login";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $countries = Country::orderBy('name', 'asc')->pluck('name', 'id');
        $states = State::orderBy('name', 'asc')->pluck('name', 'id');
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');

        $sessions = Session::orderBy('session', 'asc')->pluck('session', 'id');
        $departments = Department::orderBy('name', 'asc')->pluck('name', 'id');
        $batches = Batch::orderBy('batch_number', 'asc')->pluck('batch_number', 'id');


        $occupation_list = Occupation::orderBy('name', 'asc')->pluck('name', 'id');
        $department_batch_list = Config('constants.department_batch');
        $preferred_communication_channels = Config('constants.preferred_communication_channel');

        return view('auth.register', compact('countries', 'states', 'cities', 'sessions', 'departments',
            'batches', 'occupation_list', 'department_batch_list', 'preferred_communication_channels'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'gender' => ['required', 'string'],
            'picture' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'blood_group' => ['required', 'string'],
            'mobile_no' => ['required', 'string'],
            'department_id' => ['required', 'string'],
            'department_short_code' => ['required', 'string'],
            'session_id' => ['required', 'string'],
            'batch_id' => ['required', 'string'],
            'department_batch' => ['required', 'string'],
            'occupation' => ['required', 'string'],
            'current_organization' => ['required', 'string'],
            'present_address_line_1' => ['required', 'string'],
            'present_country_id' => ['required'],
            'present_state_id' => ['required'],
            'present_city_id' => ['required'],
            'permanent_address_line_1' => ['required_unless:sameAddress,on'],
            'permanent_country_id' => ['required_unless:sameAddress,on'],
            'permanent_state_id' => ['required_unless:sameAddress,on'],
            'permanent_city_id' => ['required_unless:sameAddress,on'],
            'preferred_communication_channel' => ['required'],
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        try {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'verify_token' => Str::random(25),
            ]);
            $fileName = $user->id . '_' . time() . '.' . $data['picture']->extension();
            Storage::disk('public')->put('uploads/picture/' . $fileName, File::get($data['picture']));
            $profile = Profile::create([
                'user_id' => $user->id,
                'name' => $data['name'],
                'gender' => $data['gender'],
                'picture' => 'uploads/picture/' . $fileName,
                'blood_group' => $data['blood_group'],
                'mobile_no' => $data['mobile_no'],
                'mobile_no_privacy' => $data['mobile_no_privacy'],
                'department_id' => $data['department_id'],
                'department_short_code' => $data['department_short_code'],
                'session_id' => $data['session_id'],
                'batch_id' => $data['batch_id'],
                'department_batch' => $data['department_batch'],
                'registration_no' => $data['registration_no'],
                'occupation' => $data['occupation'],
                'occupation_id' => $data['occupation'],
                'current_organization' => $data['current_organization'],
                'designation' => $data['designation'],
                'present_address_line_1' => $data['present_address_line_1'],
                'present_address_line_2' => $data['present_address_line_2'],
                'present_country_id' => $data['present_country_id'],
                'present_state_id' => $data['present_state_id'],
                'present_city_id' => $data['present_city_id'],
                'permanent_address_line_1' => $data['permanent_address_line_1'],
                'permanent_address_line_2' => $data['permanent_address_line_2'],
                'permanent_country_id' => $data['permanent_country_id'],
                'permanent_state_id' => $data['permanent_state_id'],
                'permanent_city_id' => $data['permanent_city_id'],
                'linkedIn_link' => $data['linkedIn_link'],
                'facebook_link' => $data['facebook_link'],
                'twitter_link' => $data['twitter_link'],
                'whatsApp_no' => $data['whatsApp_no'],
                'viber_no' => $data['viber_no'],
                'preferred_communication_channel' => $data['preferred_communication_channel'],
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            Log::error($ex->getTraceAsString());
            return response()->json(['error' => $ex->getMessage()], 500);
        }

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        $request->session()->flash('confirmation', 'Task was successful!');
        return redirect($this->redirectTo);
    }

}
