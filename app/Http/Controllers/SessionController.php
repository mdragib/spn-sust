<?php

namespace App\Http\Controllers;

use App\Department;
use App\Session;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $sessions =Session::orderBy('created_at','desc')->paginate(10);

        return view('backend.session.index', compact('sessions','sl'));
    }


    public function create()
    {

        return view('backend.session.create');
    }


    public function store(Request $request)
    {
//        dd($request->all());
        try {
            $request->validate([
                'session' => ['required', 'string', 'max:255'],
                'batch_number' => ['required', 'string', 'max:255'],
            ]);
            $sessions = $request->all();

            Session::create($sessions);

            return redirect()->route('session.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(Session $session)
    {

        return view('backend.session.edit', compact('session'));
    }

    public function update(Request $request, Session $session)
    {
        try{

            $session->update($request->all());
            return redirect()->route('session.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }
}
