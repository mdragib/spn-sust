<?php

namespace App\Http\Controllers;

use App\Country;
use App\Division;
use App\State;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $states =State::orderBy('created_at','desc')->paginate(10);

        return view('backend.state.index', compact('states','sl'));
    }


    public function create()
    {
        $countries = Country::pluck('name', 'id');
        return view('backend.state.create', compact('countries'));
    }


    public function store(Request $request)
    {
//        dd($request->all());
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
            ]);
            $states = $request->all();

            State::create($states);

            return redirect()->route('state.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(State $state)
    {
        $countries = Country::pluck('name', 'id');

        return view('backend.state.edit', compact('state', 'countries'));
    }


    public function update(Request $request, State $state)
    {
        try{

            $state->update($request->all());
            return redirect()->route('state.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }


}
