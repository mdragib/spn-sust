<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $countries =Country::orderBy('created_at','desc')->paginate(10);

        return view('backend.country.index', compact('countries','sl'));
    }


    public function create()
    {

        return view('backend.country.create');
    }


    public function store(Request $request)
    {
//        dd($request->all());
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
            ]);
            $countries = $request->all();

            Country::create($countries);

            return redirect()->route('country.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(Country $country)
    {

        return view('backend.country.edit', compact('country'));
    }

    public function update(Request $request, Country $country)
    {
        try{

            $country->update($request->all());
            return redirect()->route('country.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }
}
