<?php

namespace App\Http\Controllers;

use App\Batch;
use App\City;
use App\Country;
use App\Department;
use App\Profile;
use App\Session;
use App\State;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }
//    public function myProfile()
//    {
//        $countries = Country::pluck('name', 'id');
//        $states = State::pluck('name', 'id');
//        $cities = City::pluck('name', 'id');
//
//        $sessions = Session::pluck('session', 'id');
//        $departments = Department::pluck('name', 'id');
//        $batches = Batch::pluck('batch_number', 'id');
//
//
//        return view('auth.register', compact('countries', 'states', 'cities', 'sessions', 'departments', 'batches'));
//    }

    public function getPendingEmail(Request $request)
    {
        $users = Profile::with('user', 'department', 'session', 'batch', 'country', 'state', 'city', 'occupationModel')
            ->whereHas('user', function ($q) {
                $q->where('email_verified_at', null);
            })->get();
        return view('backend.email-verify', compact('users'));
    }

    public function approveEmail(Request $request)
    {
        $params = $request->all();
        try {
            $user = User::where('id', $params['id'])->where('email_verified_at', null)->firstOrFail();
            $user->email_verified_at = Carbon::now();
            $user->save();
            return Redirect::to("/pending/emails")->withSuccess('User email verified successfully.');
        } catch (ModelNotFoundException $exception) {
            return Redirect::to("/pending/emails")->withFail('Invalid email.');
        } catch (\Exception $exception) {
            return Redirect::to("/pending/emails")->withFail('Unable to update email.');
        }
    }

    public function activeUser(Request $request)
    {
        $users = Profile::with('user', 'department', 'session', 'batch', 'country', 'state', 'city', 'occupationModel')
            ->whereHas('user', function ($q) {
                $q->where('status', 1);
            })->get();
        return view('backend.active-users', compact('users'));
    }

    public function deactivateAccount(Request $request)
    {
//        dd('hello');
        $params = $request->all();
        try {
            $user = User::where('id', $params['id'])->where('status', 1)->firstOrFail();
            $user->status = 3 ;
            $user->save();
            return Redirect::to("/active/users")->withSuccess('User account deactivate successfully.');
        } catch (ModelNotFoundException $exception) {
            return Redirect::to("/active/users")->withFail('Invalid account.');
        } catch (\Exception $exception) {
            return Redirect::to("/active/users")->withFail('Unable to update account.');
        }
    }

    public function deactivateUser(Request $request)
    {
        $users = Profile::with('user', 'department', 'session', 'batch', 'country', 'state', 'city', 'occupationModel')
            ->whereHas('user', function ($q) {
                $q->where('status', 3);
            })->get();
        return view('backend.deactivate-users', compact('users'));
    }

    public function activateAccount(Request $request)
    {
//        dd('hello');
        $params = $request->all();
        try {
            $user = User::where('id', $params['id'])->where('status', 3)->firstOrFail();
            $user->status = 1 ;
            $user->save();
            return Redirect::to("/deactivate/users")->withSuccess('User account active successfully.');
        } catch (ModelNotFoundException $exception) {
            return Redirect::to("/deactivate/users")->withFail('Invalid account.');
        } catch (\Exception $exception) {
            return Redirect::to("/deactivate/users")->withFail('Unable to update account.');
        }
    }
}
