<?php

namespace App\Http\Controllers;


use App\Occupation;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class OccupationController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page - 1) * 10 : 0;
        $occupations = Occupation::orderBy('created_at', 'desc')->paginate(10);

        return view('backend.occupation.index', compact('occupations', 'sl'));
    }


    public function create()
    {

        return view('backend.occupation.create');
    }


    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
            ]);
            $occupation = $request->all();

            Occupation::create($occupation);

            return redirect()->route('occupation.index')->withStatus('Created successfully!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(Occupation $occupation)
    {
        return view('backend.occupation.edit', compact('occupation'));
    }

    public function update(Request $request, Occupation $occupation)
    {
        try {
            $occupation->update($request->all());
            return redirect()->route('occupation.index')->withStatus('Updated successfully!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }
}
