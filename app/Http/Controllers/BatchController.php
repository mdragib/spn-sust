<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\Session;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $batches =Batch::orderBy('created_at','desc')->paginate(10);

        return view('backend.batch.index', compact('batches','sl'));
    }


    public function create()
    {
        $sessions = Session::pluck('session',  'id');
        $departments = Department::pluck('name', 'id');
//      dd($sessions);
        return view('backend.batch.create', compact('sessions', 'departments'));
    }


    public function store(Request $request)
    {
//        dd($request->all());
        try {
            $request->validate([
                'batch_number' => ['required', 'string', 'max:255'],
            ]);
            $batches = $request->all();

            Batch::create($batches);

            return redirect()->route('batch.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(Batch $batch)
    {
        $sessions = Session::pluck('session', 'id');
        $departments = Department::pluck('name', 'id');


        return view('backend.batch.edit', compact('batch', 'sessions', 'departments'));
    }


    public function update(Request $request, Batch $batch)
    {
        try{

            $batch->update($request->all());
            return redirect()->route('batch.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()-> withErrors($e->getMessage());
        }

    }
}
