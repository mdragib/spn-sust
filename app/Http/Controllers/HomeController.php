<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\DataExportLog;
use App\Department;
use App\Exports\UsersExport;
use App\Occupation;
use App\Profile;
use App\Session;
use App\State;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $stats = $this->getDashboardStats();
        $data = $this->getFilterData();
        return view('backend.dashboard', compact('data', 'stats'));
    }

    public function postDashboard(Request $request)
    {
        $params = $request->all();
        $users = $this->getUserList($params, true);
        $stats = $this->getDashboardStats();
        $export = base64_encode(json_encode($params));
        $data = $this->getFilterData();
        return view('backend.dashboard', compact('data', 'users', 'export', 'params', 'stats'));
    }

    private function getUserList($params, $isAdmin)
    {
        $query = Profile::query();

        if (isset($params['department']) && !empty($params['department'])) {
            $query->where('department_id', $params['department']);
        }

        if (isset($params['session']) && !empty($params['session'])) {
            $query->where('session_id', $params['session']);
        }

        if (isset($params['batch']) && !empty($params['batch'])) {
            $query->where('batch_id', $params['batch']);
        }

        if (isset($params['department_batch']) && !empty($params['department_batch'])) {
            $query->where('department_batch', $params['department_batch']);
        }

        if (isset($params['occupation']) && !empty($params['occupation'])) {
            $query->where('occupation_id', $params['occupation']);
        }

        if (isset($params['blood_group']) && !empty($params['blood_group'])) {
            $query->where('blood_group', $params['blood_group']);
        }

        if (isset($params['gender']) && !empty($params['gender'])) {
            $query->where('gender', $params['gender']);
        }

        if (isset($params['country']) && !empty($params['country'])) {
            $query->where('present_country_id', $params['country']);
        }

        if (isset($params['state']) && !empty($params['state'])) {
            $query->where('present_state_id', $params['state']);
        }

        if (isset($params['city']) && !empty($params['city'])) {
            $query->where('present_city_id', $params['city']);
        }

        if (isset($params['email']) && !empty($params['email'])) {
            $query->whereHas('user', function ($q) use ($params) {
                $q->where('email', $params['email']);
            });
        }

        if (isset($params['mobile_no']) && !empty($params['mobile_no'])) {
            $query->where('mobile_no', $params['mobile_no']);
            if (!$isAdmin) {
                $query->where('mobile_no_privacy', 'All');
            }
        }
        if (isset($params['reg_no']) && !empty($params['reg_no'])) {
            $query->where('registration_no', $params['reg_no']);
        }
        if (isset($params['preferred_communication_channel']) && !empty($params['preferred_communication_channel'])) {
            $query->where('preferred_communication_channel', $params['preferred_communication_channel']);
        }
        $query->whereHas('user', function ($q) {
            $q->where('status', 1);
        });

        return $query->with('user', 'department', 'session', 'batch', 'country', 'state', 'city', 'occupationModel')->get();
    }

    private function getFilterData()
    {
        $data['country'] = Country::orderBy('name', 'asc')->pluck('name', 'id');
        $data['state'] = State::orderBy('name', 'asc')->pluck('name', 'id');
        $data['city'] = City::orderBy('name', 'asc')->pluck('name', 'id');
        $data['session'] = Session::orderBy('session', 'asc')->pluck('session', 'id');
        $data['department'] = Department::orderBy('name', 'asc')->pluck('name', 'id');
        $data['batch'] = Session::pluck('batch_number', 'id');
        $data['occupation'] = Occupation::orderBy('name', 'asc')->pluck('name', 'id');
        $data['department_batch'] = Config('constants.department_batch');
        $data['blood_group'] = Config('constants.blood_group');
        $data['gender'] = Config('constants.gender');
        $data['preferred_communication_channel'] = Config('constants.preferred_communication_channel');
        return $data;
    }

    private function getDashboardStats()
    {
        if (Auth::user()->role != 'ADMIN') {
            return [];
        }
        $data['totalUser'] = User::count();
        $data['newUser'] = User::where('created_at', '>=', Carbon::today())->count();
        $data['pendingUser'] = User::where('status', 0)->count();
        $data['totalAdmin'] = User::where('role', 'ADMIN')->count();
        $data['pendingEmail'] = User::where('email_verified_at', null)->count();

        return $data;
    }

    public function profile($id)
    {
        $user = Profile::where('id', $id)->with('user', 'department', 'session', 'batch', 'country', 'state', 'city', 'occupationModel')->first();
        return view('backend.profile', compact('user'));
    }

    public function export($data)
    {
        $log = new DataExportLog();
        $log->export_by = "User id:" . Auth::user()->id . "| Email: " . Auth::user()->email . "| Name: " . Auth::user()->name;
        $params = json_decode(base64_decode($data), true);
        $users = $this->getUserList($params, true);
        $log->desc = "Total " . count($users) . " user info exported";
        $log->save();
        return Excel::download(new UsersExport($users), 'users.xlsx');
    }

    public function getPendingUser(Request $request)
    {
        $users = Profile::with('user', 'department', 'session', 'batch', 'country', 'state', 'city', 'occupationModel')
            ->whereHas('user', function ($q) {
                $q->where('status', 0);
            })->get();
        return view('backend.approval', compact('users'));
    }

    public function approveUser(Request $request)
    {
        $params = $request->all();
        try {
            $user = User::where('id', $params['id'])->where('status', 0)->firstOrFail();
            $user->status = 1;
            $user->save();
            return Redirect::to("/pending/users")->withSuccess('User registration approved successfully.');
        } catch (ModelNotFoundException $exception) {
            return Redirect::to("/pending/users")->withFail('Invalid user.');
        } catch (\Exception $exception) {
            return Redirect::to("/pending/users")->withFail('Unable to update user.');
        }
    }

    public function declineUser(Request $request)
    {
        $params = $request->all();
        try {
            $user = User::where('id', $params['id'])->where('status', 0)->firstOrFail();
            $user->status = 2;
            $user->save();
            return Redirect::to("/pending/users")->withSuccess('User registration declined.');
        } catch (ModelNotFoundException $exception) {
            return Redirect::to("/pending/users")->withFail('Invalid user.');
        } catch (\Exception $exception) {
            return Redirect::to("/pending/users")->withFail('Unable to update user.');
        }
    }


}
