<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\State;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $cities =City::orderBy('created_at','desc')->paginate(10);

        return view('backend.city.index', compact('cities','sl'));
    }


    public function create()
    {
        $countries = Country::pluck('name', 'id');
        $states = State::pluck('name', 'id');

        return view('backend.city.create', compact('countries', 'states'));
    }


    public function store(Request $request)
    {
//        dd($request->all());
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
            ]);
            $cities = $request->all();

            City::create($cities);

            return redirect()->route('city.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(City $city)
    {
        $countries = Country::pluck('name', 'id');
        $states = State::pluck('name', 'id');


        return view('backend.city.edit', compact('city', 'countries', 'states'));
    }


    public function update(Request $request, City $city)
    {
        try{

            $city->update($request->all());
            return redirect()->route('city.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()-> withErrors($e->getMessage());
        }

    }


}
