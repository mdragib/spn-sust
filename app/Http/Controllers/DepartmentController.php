<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $departments =Department::orderBy('created_at','desc')->paginate(10);

        return view('backend.department.index', compact('departments','sl'));
    }


    public function create()
    {

        return view('backend.department.create');
    }


    public function store(Request $request)
    {
//        dd($request->all());
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'department_short_code'=>['required', 'string', 'max:255'],
            ]);
            $departments = $request->all();

            Department::create($departments);

            return redirect()->route('department.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(Department $department)
    {

        return view('backend.department.edit', compact('department'));
    }

    public function update(Request $request, Department $department)
    {
        try{

            $department->update($request->all());
            return redirect()->route('department.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }
}
