<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class ApprovedUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->status == 1) {
            return $next($request);
        }
        elseif ($request->user() && $request->user()->status == 3){
            return new Response(view('auth.deactivate'));

        }

        return new Response(view('auth.unapproved'));

    }
}
