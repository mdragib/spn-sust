<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name', 'department_short_code'];

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}
