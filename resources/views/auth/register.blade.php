<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> SUSTIAN Professional Network</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('ui/backend/')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('ui/backend/')}}/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="{{asset('ui/backend/')}}/css/custom.css" rel="stylesheet">


</head>

<body>

<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                {{--                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>--}}
                <div class="col-md-12 register-form">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Welcome to the NETWORK of EFFICIENCY!</h1>
                        </div>
                        {{--                        <form class="user">--}}
                        {{ Form::open(['route'=>'register',
                      'files'=>true,
                      'class'=>'user'
                      ])}}
                        <div class="form-group ">
                            <label for="fullNmae">Full Name*</label>
                            <input id="name" type="text"
                                   class="form-control form-control-user @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                   placeholder="Your Full Name">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {{Form::label('picture','Profile Photo*')}}<br>
                            <input name="picture" type="file" id="picture" class="form-control" required/>

                            @error('picture')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="gender"> Gender*</label>
                                <select class="form-control form-control-user" name="gender" required>
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="bloodGroup">Blood Group*</label>
                                <select class="form-control form-control-user" name="blood_group" required>
                                    <option value="">Select Blood Group</option>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                </select>
                                @error('blood_group')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="emailAddress">Email*</label>
                            <input id="email" type="email"
                                   class="form-control form-control-user @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email"
                                   placeholder="Your Valid Email Address">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="mobileNo">Mobile No.*</label>
                                <input name="mobile_no" class="form-control form-control-user " type="text"
                                       required placeholder="Your Mobile No.">
                                @error('mobile_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="mobile_no_privacy"> Mobile No. Privacy*</label>
                                <select class="form-control form-control-user" name="mobile_no_privacy">
                                    <option value="Admin">Show to Admin Only</option>
                                    <option value="All">Show to Other Members</option>
                                </select>
                                @error('mobile_no_privacy')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{--                                <div class="form-group col-md-3 form-check ">--}}
                        {{--                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">--}}
                        {{--                                    <label class="form-check-label" for="defaultCheck1">--}}
                        {{--                                        Show to Other Member--}}
                        {{--                                    </label>--}}
                        {{--                                </div>--}}

                        {{--                            </div>--}}

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{Form::label('departmentSelect','Department*')}}<br>
                                {{Form::select('department_id',$departments, null,
                                       ['class'=>'form-control form-control-user',
                                       'id'=>'departmentSelect',
                                       'placeholder'=>'Select One',
                                       'required',
                                       ])}}
                                @error('department_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                {{Form::label('departmentShortCodeSelect','Department Short Code*')}}<br>
                                {{Form::select('department_short_code',[], null,
                                       ['class'=>'form-control form-control-user',
                                       'id'=>'departmentShortCodeSelect',
                                       'required',
                                       ])}}
                                @error('department_short_code')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{Form::label('sessionSelect','Session*')}}<br>
                                {{Form::select('session_id',$sessions, null,
                                       ['class'=>'form-control form-control-user',
                                       'id'=>'sessionSelect',
                                       'placeholder'=>'Select One',
                                       'required',
                                       ])}}
                                @error('session_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                {!! Form::label('batchSelect', 'Batch*'); !!}
                                {!! Form::select('batch_id', [], null,
                                ['class'=>'form-control form-control-user',
                                'id'=>'batchSelect',
                                'required',
                                ])!!}
                                @error('batch_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="departmentBatch">Department Batch*</label>
                                <select class="form-control form-control-user" name="department_batch" required>
                                    @foreach ($department_batch_list as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                                @error('department_batch')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="registrationNo">Registration No.</label>

                                <input name="registration_no" class="form-control form-control-user"
                                       type="text"
                                       placeholder="">
                                @error('registration_no')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group ">
                            <label for="occupation">Occupation*</label>
                            <select class="form-control form-control-user" name="occupation" required>
                                @foreach ($occupation_list as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>

                            @error('occupation')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="currentOrganization">Current Organization*</label>
                                <input name="current_organization" class="form-control form-control-user"
                                       type="text"
                                       placeholder="" required>
                                @error('current_organization')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="designation">Designation</label>
                                <input name="designation" class="form-control form-control-user"
                                       type="text"
                                       placeholder="e.g. Manager "
                                       required>
                                @error('designation')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>

                        </div>

                        <fieldset>
                            <span> <big> Present Address:</big></span>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="addressLine1">Address Line 1*</label>

                                    <input name="present_address_line_1" class="form-control form-control-user"
                                           type="text"
                                           placeholder="" required>
                                    @error('present_address_line_1')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="addressLine2">Address Line 2</label>

                                    <input name="present_address_line_2" class="form-control form-control-user"
                                           type="text"
                                           placeholder=" ">
                                    @error('present_address_line_2')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    {!! Form::label('countrySelect', 'Country*'); !!}
                                    {!! Form::select('present_country_id', $countries, null,
                                    ['class'=>'form-control form-control-user',
                                    'id'=>'countrySelect',
                                    'required',
                                    'placeholder'=>'Select One',
                                    ]) !!}

                                    @error('present_country_id')
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-4">
                                    {!! Form::label('stateSelect', 'State*'); !!}
                                    {!! Form::select('present_state_id', [], null, [
                                    'class'=>'form-control form-control-user',
                                    'id'=>'stateSelect',
                                    'required',
                                    ]) !!}

                                    @error('present_state_id')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-4">
                                    {!! Form::label('citySelect', 'City*'); !!}
                                    {!! Form::select('present_city_id', [], null, [
                                    'class'=>'form-control form-control-user',
                                    'id'=>'citySelect',
                                    'required',]) !!}

                                    @error('present_city_id')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                            </div>

                        </fieldset>
                        <br>
                        <fieldset>
                            <span> <big> Permanent Address:</big></span>


                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="on" name="sameAddress"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Same as Present Address
                                </label>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="addressLine1">Address Line 1*</label>

                                    <input name="permanent_address_line_1" class="form-control form-control-user"
                                           type="text"
                                           placeholder=" ">
                                    @error('permanent_address_line_1')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="addressLine2">Address Line 2</label>

                                    <input name="permanent_address_line_2" class="form-control form-control-user"
                                           type="text" id="addressline2" value=""
                                           placeholder=" ">
                                    @error('permanent_address_line_2')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    {!! Form::label('permanentCountrySelect', 'Country*'); !!}
                                    {!! Form::select('permanent_country_id', $countries, null,
                                    ['class'=>'form-control form-control-user',
                                    'id'=>'permanentCountrySelect',
                                    'required','placeholder'=>'Select One',
                                    ]) !!}
                                    @error('permanent_country_id')
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-4">
                                    {!! Form::label('permanentStateSelect', 'State*'); !!}
                                    {!! Form::select('permanent_state_id', [], null, [
                                    'class'=>'form-control form-control-user',
                                    'id'=>'permanentStateSelect',
                                    'required',
                                    ]) !!}

                                    @error('permanent_state_id')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-4">
                                    {!! Form::label('permanentCitySelect', 'City*'); !!}
                                    {!! Form::select('permanent_city_id', [], null, [
                                    'class'=>'form-control form-control-user',
                                    'id'=>'permanentCitySelect',
                                    'required',
                                    ]) !!}
                                    @error('permanent_city_id')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                            </div>

                        </fieldset>

                        <br>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="linkedIn">LinkedIn </label>
                                <input name="linkedIn_link" class="form-control form-control-user " type="text"
                                       placeholder="Your LinkedIn Profile Link">
                                @error('linkedIn_link')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="facebook">Facebook</label>
                                <input name="facebook_link" class="form-control form-control-user " type="text"
                                       placeholder="Your Facebook Profile Link">
                                @error('facebook_link')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="twitter">Twitter</label>
                            <input name="twitter_link" class="form-control form-control-user " type="text"
                                   placeholder="Your Twitter Profile Link">
                            @error('twitter_link')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="whatsAppNo">WhatsApp No.</label>
                                <input name="whatsApp_no" class="form-control form-control-user " type="text"
                                       placeholder=" ">
                                @error('whatsApp_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="viberNo">Viber No.</label>
                                <input name="viber_no" class="form-control form-control-user " type="text">
                                @error('viber_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="preferredCommunicationChannel">Preferred Communication Channel*</label>
                            <select class="form-control form-control-user" name="preferred_communication_channel"
                                    required>
                                @foreach ($preferred_communication_channels as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                            @error('preferred_communication_channel')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        <hr>
                        <a href="{{ route('password.request') }}" class="btn btn-google btn-user btn-block">
                            Forgot Password?
                        </a>
                        <a href="{{url('/login')}} " class="btn btn-facebook btn-user btn-block">
                            Already have an account? Login
                        </a>
                        {{ Form::close()}}
                        {{--                        </form>--}}
                        <hr>
                        <div class="text-center">
                            This is a common platform of SUSTIANs,
                            where people from SUST of different professions can engage to each other.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer footer-alt footer">
    <span class="text-white" target="blank">Developed by</span> <span style="text-decoration: underline;"><a
                href="https://ogroni.com/" class="text-white" target="blank">Ogroni Informatix Limited</a></span>
</footer>

<!-- Bootstrap core JavaScript-->
<script src="{{asset('ui/backend/')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('ui/backend/')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('ui/backend/')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('ui/backend/')}}/js/sb-admin-2.min.js"></script>

<script>
    $("#countrySelect").change(function () {
        var country = $("#countrySelect").val();
        $("#stateSelect").html("")

        var option = "";

        $.get("/get-states/" + country,
            function (data) {
                data = JSON.parse(data)
                option += "<option value=' '>Select State</option>";
                data.forEach(function (element) {

                    option += "<option value='" + element.id + "'>" + element.name + "</option>";
                });
                $("#stateSelect").html(option)

            })
    })

    $("#stateSelect").change(function () {
        var state = $("#stateSelect").val();
        $("#citySelect").html("")

        var option = "";

        $.get("/get-cities/" + state,
            function (data) {
                data = JSON.parse(data)
                option += "<option value=' '>Select City</option>";
                data.forEach(function (element) {

                    option += "<option value='" + element.id + "'>" + element.name + "</option>";
                });
                $("#citySelect").html(option)


            })

    })


    $("#permanentCountrySelect").change(function () {
        var country = $("#permanentCountrySelect").val();
        $("#permanentStateSelect").html("")

        var option = "";

        $.get("/get-states/" + country,
            function (data) {
                data = JSON.parse(data)
                data.forEach(function (element) {

                    option += "<option value='" + element.id + "'>" + element.name + "</option>";
                });
                $("#permanentStateSelect").html(option)

            })
    })

    $("#permanentStateSelect").change(function () {
        var state = $("#permanentStateSelect").val();
        $("#permanentCitySelect").html("")

        var option = "";

        $.get("/get-cities/" + state,
            function (data) {
                data = JSON.parse(data)

                data.forEach(function (element) {

                    option += "<option value='" + element.id + "'>" + element.name + "</option>";
                });
                $("#permanentCitySelect").html(option)


            })

    })


    $("#sessionSelect").change(function () {
        var session = $("#sessionSelect").val();
        $("#batchSelect").html("")

        var option = "";

        $.get("/get-batches/" + session,
            function (data) {
                data = JSON.parse(data)

                data.forEach(function (element) {

                    option += "<option value='" + element.id + "'>" + element.batch_number + "</option>";

                });
                $("#batchSelect").html(option)

            })
    })


    $("#departmentSelect").change(function () {
        var department = $("#departmentSelect").val();
        $("#departmentShortCodeSelect").html("")

        var option = "";

        $.get("/get-department-short-codes/" + department,
            function (data) {
                data = JSON.parse(data)
                data.forEach(function (element) {
                    // console.log(element);
                    option += "<option value='" + element.id + "'>" + element.department_short_code + "</option>";

                });
                $("#departmentShortCodeSelect").html(option)

            })

    })

    $("#defaultCheck1").change(function () {
        if (this.checked) {
            $("[name='permanent_address_line_1']").val($("[name='present_address_line_1']").val());
            $("[name='permanent_address_line_2']").val($("[name='present_address_line_2']").val());
            $("[name='permanent_country_id']").val($("[name='present_country_id']").val());
            $('#permanentStateSelect').empty();
            $('#stateSelect option').clone().appendTo('#permanentStateSelect');
            $("[name='permanent_state_id']").val($("[name='present_state_id']").val());
            $('#permanentCitySelect').empty();
            $('#citySelect option').clone().appendTo('#permanentCitySelect');
            $("[name='permanent_city_id']").val($("[name='present_city_id']").val());


        } else {
            $("[name='permanent_address_line_1']").val("");
            $("[name='permanent_address_line_2']").val("");
            $("[name='permanent_country_id']").val("");
            $("[name='permanent_state_id']").val("");
            $("[name='permanent_city_id']").val("");
        }
    });

</script>

{{--<script>--}}
{{--    $("#stateSelect").change(function () {--}}
{{--        var state = $("#stateSelect").val();--}}
{{--        $("#citySelect").html("")--}}

{{--        var option = "";--}}

{{--        $.get("http://spn-sust.test/get-cities/"+state,--}}
{{--            function (data) {--}}
{{--                data = JSON.parse(data)--}}

{{--                data.forEach(function (element) {--}}

{{--                    option += "<option value='"+ element.id +"'>"+ element.name +"</option>";--}}
{{--                });--}}
{{--                $("#citySelect").html(option)--}}


{{--            })--}}

{{--    })--}}
{{--</script>--}}

{{--<script>--}}
{{--    $("#sessionSelect").change(function () {--}}
{{--        var session = $("#sessionSelect").val();--}}
{{--        $("#batchSelect").html("")--}}

{{--        var option = "";--}}

{{--        $.get("http://spn-sust.test/get-batches/"+session,--}}
{{--            function (data) {--}}
{{--                data = JSON.parse(data)--}}

{{--                data.forEach(function (element) {--}}

{{--                    option += "<option value='"+ element.id +"'>"+ element.batch_number +"</option>";--}}

{{--                });--}}
{{--                $("#batchSelect").html(option)--}}

{{--            })--}}
{{--    })--}}
{{--</script>--}}

{{--<script>--}}
{{--    $("#departmentSelect").change(function () {--}}
{{--        var department = $("#departmentSelect").val();--}}
{{--        $("#departmentShortCodeSelect").html("")--}}

{{--        var option = "";--}}

{{--        $.get("http://spn-sust.test/get-department-short-codes/"+department,--}}
{{--            function (data) {--}}
{{--                data = JSON.parse(data)--}}
{{--                data.forEach(function (element) {--}}
{{--                    // console.log(element);--}}
{{--                    option += "<option value='"+ element.id +"'>"+ element.department_short_code +"</option>";--}}

{{--                });--}}
{{--                $("#departmentShortCodeSelect").html(option)--}}

{{--            })--}}

{{--    })--}}
{{--</script>--}}

</body>

</html>


