@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Account activate required') }}</div>

                    <div class="card-body">
                        {{ __('You have successfully registered to the network of efficiency! Your account will be activated by an Admin/Moderator manually.') }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


