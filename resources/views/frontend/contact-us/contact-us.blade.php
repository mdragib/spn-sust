@extends('frontend.layouts.master')

@section('title','Contact Us')

@section('content')

    <p style="text-align: center; color: white">We are always ready to support you! Please feel free to write us on info@sustian.com.bd</p>

@endsection
