@push('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
@endpush

{{--<nav class="navbar navbar-expand-sm navbar-light " >--}}
{{--    <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20"--}}
{{--            aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--        <div class="animated-icon1"><span></span><span></span><span></span></div>--}}
{{--    </button>--}}
{{--    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--        <span class="navbar-toggler-icon"></span>--}}
{{--    </button>--}}
{{--    <div class="collapse navbar-collapse" id="navbarTogglerDemo01" >--}}
{{--        <ul class="navbar-nav mr-auto mt-2 mt-sm-0 navbar-logo mx-auto">--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{url('/news-and-events')}}">News and Events </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{url('/about-us')}}">About Us </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{url('/privacy-policy')}}">Privacy Policy </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{url('/contact-us')}}">Contact Us </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{url('/blog')}}">Blog </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{route('login')}}">Login </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item ">--}}
{{--                <a style="color: white" class="nav-link" href="{{route('register')}}">Register </a>--}}
{{--            </li>--}}

{{--        </ul>--}}

{{--    </div>--}}
{{--</nav>--}}


<nav class="navbar navbar-expand-sm navbar-dark bg-dark" style="background-color:transparent!important">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
{{--        <a class="navbar-brand" href="#">Hidden brand</a>--}}
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0 mx-auto">
            <li class="nav-item active">
                <a style="color: white" class="nav-link" href="{{url('/news-and-events')}}">News and Events <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a style="color: white" class="nav-link" href="{{url('/about-us')}}">About Us</a>
            </li>
            <li class="nav-item ">
                <a style="color: white" class="nav-link" href="{{url('/privacy-policy')}}">Privacy Policy </a>
            </li>
            <li class="nav-item ">
                <a style="color: white" class="nav-link" href="{{url('/contact-us')}}">Contact Us </a>
            </li>
            <li class="nav-item ">
                <a style="color: white" class="nav-link" href="{{url('/blog')}}">Blog </a>
            </li>
            <li class="nav-item ">
                <a style="color: white" class="nav-link" href="{{url('/login')}}">Login </a>
            </li>
            <li class="nav-item ">
                <a style="color: white" class="nav-link" href="{{url('/register')}}">Register </a>
            </li>
        </ul>
    </div>
</nav>


@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endpush
