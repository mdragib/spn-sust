<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title') </title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('ui/backend/')}}/vendor/fontawesome-free/{{asset('ui/backend/')}}/css/all.min.css"
          rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template-->

    <link href="{{asset('ui/backend/')}}/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="{{asset('ui/backend/')}}/css/custom.css" rel="stylesheet">
    @stack('css')

</head>

<body>

@include('frontend.layouts.partials.navbar')

@yield('content')

@include('frontend.layouts.partials.footer')

<!-- Bootstrap core JavaScript-->
<script src="{{asset('ui/backend/')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('ui/backend/')}}/vendor/bootstrap/{{asset('ui/backend/')}}/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('ui/backend/')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

{{--<!-- Page level plugins -->--}}
{{--<script src="{{asset('ui/backend/')}}/vendor/chart.js/Chart.min.js"></script>--}}

<!-- Custom scripts for all pages-->
<script src="{{asset('ui/backend/')}}/js/sb-admin-2.min.js"></script>
@stack('script')
</body>

</html>


