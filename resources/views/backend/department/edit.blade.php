@extends('backend.layouts.master')

@section('title', 'Department Edit')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Edit Department</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('department.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{Form::model($department, [
                      'route'=>['department.update',
                      $department->id],
                      'files'=>true,
                      'method'=>'put',
                 ])}}
                @csrf
                @method('PUT')

                @include('backend.department.form')

                {{Form::button('Update',[
                                'class'=>'btn btn-primary',
                                'type'=>'submit',
                                   ])}}
                {{ Form::close()}}
            </div>
        </div>
    </div>
@endsection

