            <div class="form-row">
                <div class="col-md-12 mb-3">
                    {{Form::label('name','Department')}}
                    {{Form::text('name', null, [
                          'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control',
                          'placeholder'=>'Enter The department name',
                          'id'=>'name',
                    ])}}
                    @error('name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    {{Form::label('department_short_code','Department Short Code')}}
                    {{Form::text('department_short_code', null, [
                          'class'=> $errors->has('department_short_code') ?  'form-control is-invalid': 'form-control',
                          'placeholder'=>'Enter The department short code name',
                          'id'=>'department_short_code',
                    ])}}
                    @error('department_short_code')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>
            </div>

