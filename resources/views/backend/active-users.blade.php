@extends('backend.layouts.master')

@section('title', 'Dashboard')
@push('style')
    <link href="{{asset('ui/backend/')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{asset('ui/backend/')}}/vendor/datatables/responsive.dataTables.min.css" rel="stylesheet">
    <link href="{{asset('ui/backend/')}}/vendor/select2/select2.min.css" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('fail'))
            <div class="alert alert-danger">
                {{Session::get('fail')}}
            </div>
        @endif
        <div class="row">
            <div class="col-xl-12 col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Active Users List</h6>
                    </div>

                    <div class="card-body">
                        @if (isset($users) && count($users) > 0)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="pendingUserListTable" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Name<br/>Email Address<br/>Mobile no.</th>
                                        <th>Department<br/>Session<br/>Batch<br/>Department Batch<br/>Registration No
                                        </th>
                                        <th>Occupation<br/>Organization<br/>Designation</th>
                                        <th>Current Address</th>
                                        <th>Actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($users as $user)
                                        <tr>
                                            <td>
                                                <a href="{{ url('member/' . $user->id) }}" target="_blank"
                                                   class="text-black-50">{{ $user->user->name }}</a><br/>
                                                {{ $user->user->email }}<br/>
                                                {{ $user->mobile_no }}
                                            </td>
                                            <td>
                                                {{ $user->department->name }}<br/>
                                                {{ $user->session->session }}<br/>
                                                {{ $user->batch->batch_number }}<br/>
                                                {{ $user->department_batch }}<br/>
                                                {{ $user->registration_no }}<br/>
                                            </td>
                                            <td>
                                                {{ $user->occupationModel->name }}<br/>
                                                {{ $user->current_organization }}<br/>
                                                {{ $user->designation }}<br/>
                                            </td>
                                            <td>
                                                {{ $user->present_address_line_1 }},<br/>
                                                {{ $user->present_address_line_2 }},<br/>
                                                {{ $user->city->name }},<br/>
                                                {{ $user->state->name }},<br/>
                                                {{ $user->country->name }}<br/>
                                            </td>
                                            <td>
                                                <form action="{{route('account_deactivate')}}" method="post"
                                                      onsubmit="return confirm('Do you really want to deactivate this account?');">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$user->user->id}}">
                                                    <button class="btn btn-sm btn-danger"><i
                                                            class="fas fa-check fa-sm"></i> Deactivate
                                                    </button>
                                                </form>

                                            </td>

                                        </tr>
                                    @empty
                                        <p>No users</p>
                                    @endforelse


                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        $(document).ready(function () {
            var table = $('#userListTable').DataTable();
        });

    </script>
@endsection

@push('script')
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/select2/select2.min.js"></script>
@endpush
@section('inline-script')
    <script>
        $(document).ready(function () {
            var table = $('#pendingUserListTable').DataTable();
        });

    </script>
@endsection




