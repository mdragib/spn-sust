            <div class="form-row">
                <div class="col-md-12 mb-3">
                    {{Form::label('session','Session')}}
                    {{Form::text('session', null, [
                          'class'=> $errors->has('session') ?  'form-control is-invalid': 'form-control',
                          'placeholder'=>'Enter The session ',
                          'id'=>'session',
                    ])}}
                    @error('session')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    {{Form::label('batch_number','Batch Number')}}
                    {{Form::text('batch_number', null, [
                          'class'=> $errors->has('batch_number') ?  'form-control is-invalid': 'form-control',
                          'placeholder'=>'Enter The batch number',
                          'id'=>'batch_number',
                    ])}}
                    @error('batch_number')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>
            </div>

