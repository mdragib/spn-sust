@extends('backend.layouts.master')

@section('title', 'Occupation')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Occupation</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('occupation.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Occupation</th>

                            <th style="width: 150px; text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(session()->has('status'))
                            <div class="alert alert-success">

                                <p>{{session('status')}}</p>

                            </div>
                        @endif

                        @foreach($occupations as $occupation)
                            <tr>
                                <td>{{++$sl}}</td>
                                <td>{{$occupation->name}}</td>
                                <td>
                                    <a href="{{ route('occupation.edit', $occupation->id) }}"
                                       class="btn btn-sm btn-outline-warning">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $occupations->links() }}
            </div>

        </div>

    </div>
@endsection

@push('css')
    <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('script')
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
@endpush

