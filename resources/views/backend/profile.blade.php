@extends('backend.layouts.master')

@section('title', 'Dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Profile</h6>

                    </div>
                    <div class="card-body">
                        @if (isset($user))
                            <div class="row">
                                <div class="col-md-4 float-right">
                                    <img class="profile-image rounded-circle" alt="user-image"
                                         src={{url('storage/'.$user->picture) }}>
                                </div>
                                <div class="col-md-8">
                                    <table border="0">
                                        <tr>
                                            <td>Name:</td>
                                            <td>{{$user->user->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Email:</td>
                                            <td>{{$user->user->email}}</td>
                                        </tr>
                                        @if(Auth::user()->role == 'ADMIN' || $user->mobile_no_privacy === 'All')
                                            <tr>
                                                <td>Mobile no:</td>
                                                <td> {{$user->mobile_no}}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td>Department:</td>
                                            <td> {{$user->department->name}}
                                                ({{$user->department->department_short_code}})
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Session:</td>
                                            <td>{{$user->session->session}}</td>
                                        </tr>
                                        <tr>
                                            <td>Batch:</td>
                                            <td>{{$user->batch->batch_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>Department Batch:</td>
                                            <td> {{$user->department_batch}}</td>
                                        </tr>
                                        <tr>
                                            <td>Registration no:</td>
                                            <td> {{$user->registration_no}}</td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{$user->gender}}</td>
                                        </tr>
                                        <tr>
                                            <td>Blood group:</td>
                                            <td> {{$user->blood_group}}</td>
                                        </tr>
                                        <tr>
                                            <td>Occupation:</td>
                                            <td> {{$user->occupationModel->name}}</td>
                                        </tr>

                                        <tr>
                                            <td>Current Organization:</td>
                                            <td> {{$user->current_organization}}</td>
                                        </tr>

                                        <tr>
                                            <td>Designation:</td>
                                            <td> {{$user->designation}}</td>
                                        </tr>
                                        <tr>
                                            <td>Present Address:</td>
                                            <td> {{$user->present_address_line_1}}{{$user->present_address_line_2}},
                                                {{$user->city->name}},
                                                {{$user->state->name}},
                                                {{$user->country->name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Permanent Address:</td>
                                            <td> {{$user->permanent_address_line_1}}{{$user->permanent_address_line_2}},
                                                {{$user->permanentCity->name}},
                                                {{$user->permanentState->name}},
                                                {{$user->permanentCountry->name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>LinkedIn:</td>
                                            <td> {{$user->linkedIn_link}}</td>
                                        </tr>

                                        <tr>
                                            <td>Facebook:</td>
                                            <td> {{$user->facebook_link}}</td>
                                        </tr>

                                        <tr>
                                            <td>Twitter:</td>
                                            <td> {{$user->twitter_link}}</td>
                                        </tr>

                                        <tr>
                                            <td>WhatsApp:</td>
                                            <td> {{$user->whatsApp_no}}</td>
                                        </tr>

                                        <tr>
                                            <td>Viber:</td>
                                            <td> {{$user->viber_no}}</td>
                                        </tr>

                                        <tr>
                                            <td>Preferred Communication Channel:</td>
                                            <td> {{$user->preferred_communication_channel}}</td>
                                        </tr>

                                        <tr>
                                            <td>Joined:</td>
                                            <td> {{$user->created_at}}</td>
                                        </tr>

                                        <tr>
                                            <td>Last updated:</td>
                                            <td> {{$user->updated_at}}</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        @endif
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection



