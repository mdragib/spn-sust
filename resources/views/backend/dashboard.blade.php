@extends('backend.layouts.master')

@section('title', 'Dashboard')
@push('style')
    <link href="{{asset('ui/backend/')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{asset('ui/backend/')}}/vendor/datatables/responsive.dataTables.min.css" rel="stylesheet">
    <link href="{{asset('ui/backend/')}}/vendor/select2/select2.min.css" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>

        @if (Auth::user()->role == 'ADMIN')
            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total
                                        Registered
                                        User
                                    </div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$stats['totalUser']}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-friends fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Registration
                                        Today
                                    </div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$stats['newUser']}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-plus fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-info shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Admin User</div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-auto">
                                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$stats['totalAdmin']}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-tie fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Pending Requests Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending
                                        Requests
                                    </div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$stats['pendingUser']}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-cog fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending
                                    Requests Emails
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$stats['pendingEmail']}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-user-cog fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
        @endif

        <div class="row">
            <div class="col-xl-12 col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Search</h6>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-3 form-group">
                                    <select name="department" class="form-control" id="department">
                                        <option value="">Department</option>
                                        @foreach ($data['department'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['department'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="session" class="form-control" id="session">
                                        <option value="">Session</option>

                                        @foreach ($data['session'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['session'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="batch" class="form-control" id="batch">
                                        <option value="">Batch</option>

                                        @foreach ($data['batch'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['batch'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="department_batch" class="form-control" id="department_batch">
                                        <option value="">Department Batch</option>
                                        @foreach ($data['department_batch'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['department_batch'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="occupation" class="form-control" id="occupation">
                                        <option value="">Occupation/Current Organization</option>
                                        @foreach ($data['occupation'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['occupation'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="blood_group" class="form-control" id="blood_group">
                                        <option value="">Blood Group</option>
                                        @foreach ($data['blood_group'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['blood_group'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="col-md-3 form-group">
                                    <select name="country" class="form-control" id="country">
                                        <option value="">Country</option>
                                        @foreach ($data['country'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['country'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="state" class="form-control" id="state">
                                        <option value="">State</option>
                                        @foreach ($data['state'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['state'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <select name="city" class="form-control" id="city">
                                        <option value="">City</option>
                                        @foreach ($data['city'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['city'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6 form-group">
                                    <select name="preferred_communication_channel" class="form-control"
                                            id="preferred_communication_channel">
                                        <option value="">Preferred Communication Channel</option>
                                        @foreach ($data['preferred_communication_channel'] as $key => $value)
                                            <option value="{{ $key }}"
                                                    @if (isset($params) && $key == $params['preferred_communication_channel'])
                                                    selected="selected"
                                                    @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 form-group"></div>
                                <div class="col-md-3 form-group">
                                    <input type="email" class="form-control" id="email" placeholder="Email Address"
                                           name="email" value="{{ $params['email'] ?? '' }}">
                                </div>

                                <div class="col-md-3 form-group">
                                    <input type="text" class="form-control" id="mobile_no" placeholder="Mobile no"
                                           name="mobile_no" value="{{$params['mobile_no'] ?? ''}}">
                                </div>

                                <div class="col-md-3 form-group">
                                    <input type="reg_no" class="form-control" id="reg_no" placeholder="Registration No."
                                           name="reg_no" value="{{$params['reg_no'] ?? ''}}">
                                </div>


                                <div class="col-md-3 form-group">
                                    <button type="submit" class="btn btn-primary float-right"><i
                                                class="fas fa-search fa-sm"></i> Search
                                    </button>

                                    @if (Auth::user()->role == 'ADMIN' && isset($users) && count($users) > 0)
                                        <a class="btn btn-primary float-left" target="_blank"
                                           href="{{url('export/'.$export)}}"><i
                                                    class="fas fa-download fa-sm"></i> Export
                                        </a>
                                    @endif
                                </div>


                            </div>
                        </form>
                        <hr/>

                        @if (isset($users) && count($users) > 0)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="userListTable" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Department<br/>Session<br/>Batch<br/>Department Batch</th>
                                        <th>Occupation</th>
                                        <th>Current Organization</th>
                                        <th>Designation</th>
                                        <th>Blood Group</th>
                                        <th>Location</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($users as $user)
                                        <tr>
                                            <td><a href="{{ url('member/' . $user->id) }}" target="_blank"
                                                   class="text-black-50">{{ $user->user->name }}</a></td>
                                            <td>
                                                {{ $user->department->name }}<br/>
                                                {{ $user->session->session }}<br/>
                                                {{ $user->batch->batch_number }}<br/>
                                                {{ $user->department_batch }}
                                            </td>
                                            <td>
                                                {{ $user->occupationModel->name }}
                                            </td>
                                            <td>{{ $user->current_organization }}</td>
                                            <td>{{ $user->designation }}</td>
                                            <td>{{ $user->blood_group }}</td>
                                            <td>
                                                {{ $user->city->name }},<br/>
                                                {{ $user->state->name }},<br/>
                                                {{ $user->country->name }}<br/>
                                            </td>

                                        </tr>
                                    @empty
                                        <p>No users</p>
                                    @endforelse


                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        $(document).ready(function () {
            var table = $('#userListTable').DataTable();
        });

    </script>
@endsection

@push('script')
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/select2/select2.min.js"></script>
@endpush
@section('inline-script')
    <script>
        $(document).ready(function () {
            var table = $('#userListTable').DataTable();

            $('#department').select2();
            $('#session').select2();
            $('#batch').select2();
            $('#department_batch').select2();
            $('#occupation').select2();
            $('#blood_group').select2();
            $('#gender').select2();
            $('#country').select2();
            $('#state').select2();
            $('#city').select2();
            $('#preferred_communication_channel').select2();
        });

    </script>
@endsection


