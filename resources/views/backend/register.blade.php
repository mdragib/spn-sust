{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}

{{--<head>--}}

{{--    <meta charset="utf-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}
{{--    <meta name="description" content="">--}}
{{--    <meta name="author" content="">--}}

{{--    <title> SUSTIAN Professional Network</title>--}}

{{--    <!-- Custom fonts for this template-->--}}
{{--    <link href="{{asset('ui/backend/')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">--}}

{{--    <!-- Custom styles for this template-->--}}
{{--    <link href="{{asset('ui/backend/')}}/css/sb-admin-2.min.css" rel="stylesheet">--}}
{{--    <link href="{{asset('ui/backend/')}}/css/custom.css" rel="stylesheet">--}}


{{--</head>--}}

{{--<body >--}}

{{--<div class="container">--}}

{{--    <div class="card o-hidden border-0 shadow-lg my-5">--}}
{{--        <div class="card-body p-0">--}}
{{--            <!-- Nested Row within Card Body -->--}}
{{--            <div class="row">--}}
{{--                --}}{{--                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>--}}
{{--                <div class="col-md-12 register-form">--}}
{{--                    <div class="p-5">--}}
{{--                        <div class="text-center">--}}
{{--                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>--}}
{{--                        </div>--}}
{{--                        <form class="user">--}}
{{--                            <div class="form-group ">--}}
{{--                                <label for="fullNmae">Full Name*</label>--}}
{{--                                <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="Your Full Name">--}}

{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="profilePhoto">Profile Photo*</label>--}}
{{--                                <input class=" form-control-file" id="example-fileinput" name="" type="file">--}}
{{--                            </div>--}}

{{--                            <div class="form-row">--}}
{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="gender"> Gender*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="gender">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                        <option value="Male">Male</option>--}}
{{--                                        <option value="Female">Female</option>--}}
{{--                                        <option value="Other">Other</option>--}}
{{--                                    </select>--}}
{{--                                    @error('gender')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="bloodGroup">Blood Group*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="blood_group">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                        <option value="A+">A+</option>--}}
{{--                                        <option value="A-">A-</option>--}}
{{--                                        <option value="B+">B+</option>--}}
{{--                                        <option value="B-">B-</option>--}}
{{--                                        <option value="O+">O+</option>--}}
{{--                                        <option value="O-">O-</option>--}}
{{--                                        <option value="AB+">AB+</option>--}}
{{--                                        <option value="AB-">AB-</option>--}}
{{--                                    </select>--}}
{{--                                    @error('blood_group')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                    <strong>{{ $message }}</strong>--}}
{{--                                </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="emailAddress">Email*</label>--}}
{{--                                <input type="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Your Valid Email Address">--}}
{{--                            </div>--}}

{{--                            <div clss="form-row">--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="mobileNo">Mobile No.*</label>--}}
{{--                                    <input name="mobile_no" class="form-control form-control-user " type="text"--}}
{{--                                           id="mobileNo" value=""--}}
{{--                                           required placeholder="Your Mobile No.">--}}
{{--                                    @error('mobile_no')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-3 form-check ">--}}
{{--                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">--}}
{{--                                    <label class="form-check-label" for="defaultCheck1">--}}
{{--                                        Show to Other Member--}}
{{--                                    </label>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                            <div class="form-row">--}}
{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="deparment">Department*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="deparment">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                        <option value="Business Administration">Business Administration</option>--}}

{{--                                    </select>--}}
{{--                                    @error('deparment')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                    <strong>{{ $message }}</strong>--}}
{{--                                </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="departmentShortCode">Department Short Code*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="department_short_code">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                        <option value="BAN">BAN</option>--}}

{{--                                    </select>--}}
{{--                                    @error('department_short_code')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-row">--}}
{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="session">Session*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="session">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                    </select>--}}
{{--                                    @error('session')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="batch">Batch*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="batch">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                    </select>--}}
{{--                                    @error('batch')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                            <div class="form-row">--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="departmentBatch">Department Batch*</label>--}}
{{--                                    <select class="form-control form-control-user"  name="department_batch">--}}
{{--                                        <option value="Select one ">Select one </option>--}}
{{--                                        <option value="1st ">1st </option>--}}
{{--                                        <option value="2nd ">2nd </option>--}}
{{--                                        <option value="3rd ">3rd </option>--}}

{{--                                    </select>--}}
{{--                                    @error('department_batch')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="registrationNo">Registration No.</label>--}}

{{--                                    <input name="registration_no" class="form-control form-control-user"--}}
{{--                                           type="text"--}}
{{--                                           placeholder=" ">--}}
{{--                                    @error('registration_no')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                            <div class="form-group ">--}}
{{--                                <label for="occupation">Occupation*</label>--}}
{{--                                <select class="form-control form-control-user"  name="occupation">--}}
{{--                                    <option value="Select one ">Select one </option>--}}
{{--                                </select>--}}

{{--                                @error('occupation')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}

{{--                            <div class="form-row">--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="currentOrganization">Current Organization*</label>--}}
{{--                                    <input name="current_organization" class="form-control form-control-user"--}}
{{--                                           type="text"--}}
{{--                                           placeholder=" ">--}}
{{--                                    @error('current_organization')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="designation">Designation</label>--}}
{{--                                    <input name="designation" class="form-control form-control-user"--}}
{{--                                           type="text"--}}
{{--                                           placeholder="e.g. Manager ">--}}
{{--                                    @error('designation')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                            <fieldset>--}}
{{--                                <span> <big> Present Address:</big></span>--}}

{{--                                <div class="form-row">--}}

{{--                                    <div class="form-group col-md-6">--}}
{{--                                        <label for="addressLine1">Address Line 1*</label>--}}

{{--                                        <input name="address_line_1" class="form-control form-control-user"--}}
{{--                                               type="text"--}}
{{--                                               placeholder=" ">--}}
{{--                                        @error('address_line_1')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group col-md-6">--}}
{{--                                        <label for="addressLine2">Address Line 2</label>--}}

{{--                                        <input name="addressline2" class="form-control form-control-user"--}}
{{--                                               type="text" id="addressline2" value=""--}}
{{--                                               placeholder=" ">--}}
{{--                                        @error('address_line_2')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-row">--}}
{{--                                    <div class="form-group col-md-4">--}}
{{--                                        <label for="country">Country*</label>--}}
{{--                                        <select class="form-control form-control-user"  name="country">--}}
{{--                                            <option value="Select one ">Select one </option>--}}
{{--                                            <option value="Bangladesh">Bangladesh </option>--}}
{{--                                        </select>--}}
{{--                                        @error('country')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group col-md-4">--}}
{{--                                        <label for="state">State*</label>--}}
{{--                                        <select class="form-control form-control-user"  name="state">--}}
{{--                                            <option value="Select one ">Select one </option>--}}
{{--                                        </select>--}}
{{--                                        @error('state')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group col-md-4">--}}
{{--                                        <label for="city">City*</label>--}}

{{--                                        <select class="form-control form-control-user"  name="city">--}}
{{--                                            <option value="Select one ">Select one </option>--}}
{{--                                        </select>--}}
{{--                                        @error('city')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                </div>--}}

{{--                            </fieldset>--}}
{{--                            <br>--}}
{{--                            <fieldset>--}}
{{--                                <span> <big> Permanent Address:</big></span>--}}


{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">--}}
{{--                                    <label class="form-check-label" for="defaultCheck1">--}}
{{--                                        Same as Present Address--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                                <div class="form-row">--}}
{{--                                    <div class="form-group col-md-6">--}}
{{--                                        <label for="addressLine1">Address Line 1*</label>--}}

{{--                                        <input name="address_line_1" class="form-control form-control-user"--}}
{{--                                               type="text"--}}
{{--                                               placeholder=" ">--}}
{{--                                        @error('address_line_1')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group col-md-6">--}}
{{--                                        <label for="addressLine2">Address Line 2</label>--}}

{{--                                        <input name="addressline2" class="form-control form-control-user"--}}
{{--                                               type="text" id="addressline2" value=""--}}
{{--                                               placeholder=" ">--}}
{{--                                        @error('address_line_2')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-row">--}}
{{--                                    <div class="form-group col-md-4">--}}
{{--                                        <label for="country">Country*</label>--}}
{{--                                        <select class="form-control form-control-user"  name="country">--}}
{{--                                            <option value="Select one ">Select one </option>--}}
{{--                                            <option value="Bangladesh">Bangladesh </option>--}}
{{--                                        </select>--}}
{{--                                        @error('country')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group col-md-4">--}}
{{--                                        <label for="state">State*</label>--}}
{{--                                        <select class="form-control form-control-user"  name="state">--}}
{{--                                            <option value="Select one ">Select one </option>--}}
{{--                                        </select>--}}
{{--                                        @error('state')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group col-md-4">--}}
{{--                                        <label for="city">City*</label>--}}

{{--                                        <select class="form-control form-control-user"  name="city">--}}
{{--                                            <option value="Select one ">Select one </option>--}}
{{--                                        </select>--}}
{{--                                        @error('city')--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                        @enderror--}}
{{--                                    </div>--}}

{{--                                </div>--}}

{{--                            </fieldset>--}}

{{--                            <br>--}}

{{--                            <div class="form-row">--}}
{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="linkedIn">LinkedIn </label>--}}
{{--                                    <input name="linkedIn" class="form-control form-control-user " type="text"--}}
{{--                                           placeholder="Your LinkedIn Profile Link">--}}
{{--                                    @error('linkedIn')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="facebook">Facebook</label>--}}
{{--                                    <input name="facebook" class="form-control form-control-user " type="text"--}}
{{--                                           placeholder="Your Facebook Profile Link">--}}
{{--                                    @error('facebook')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group ">--}}
{{--                                <label for="twitter">Twitter</label>--}}
{{--                                <input name="facebook" class="form-control form-control-user " type="text"--}}
{{--                                       placeholder="Your Twitter Profile Link">--}}
{{--                                @error('twitter')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}

{{--                            <div class="form-row">--}}
{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="whatsAppNo">WhatsApp No.</label>--}}
{{--                                    <input name="mobile_no" class="form-control form-control-user " type="text"--}}
{{--                                           id="whatsAppNo" value=""--}}
{{--                                           required placeholder=" ">--}}
{{--                                    @error('whatsApp_no')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="form-group col-md-6">--}}
{{--                                    <label for="viberNo">Viber No.</label>--}}
{{--                                    <input name="mobile_no" class="form-control form-control-user " type="text"--}}
{{--                                           id="viberNo" value=""--}}
{{--                                           required placeholder=" ">--}}
{{--                                    @error('viber_no')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="preferredCommunicationChannel">Preferred Communication Channel*</label>--}}
{{--                                <select class="form-control form-control-user"  name="blood_group">--}}
{{--                                    <option value=" ">Select one </option>--}}
{{--                                    <option value="Email">Email</option>--}}
{{--                                    <option value="Phone-">Phone</option>--}}
{{--                                    <option value="LinkedIn">LinkedIn</option>--}}
{{--                                    <option value="Facebook">Facebook</option>--}}
{{--                                    <option value="Twitter">Twitter</option>--}}
{{--                                    <option value="WhatsApp">WhatsApp</option>--}}
{{--                                    <option value="Viber">Viber</option>--}}

{{--                                </select>--}}
{{--                                @error('blood_group')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                    <strong>{{ $message }}</strong>--}}
{{--                                </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}

{{--                            <div class="form-group row">--}}
{{--                                <div class="col-sm-6 mb-3 mb-sm-0">--}}
{{--                                    <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">--}}
{{--                                </div>--}}
{{--                                <div class="col-sm-6">--}}
{{--                                    <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <a href="" class="btn btn-primary btn-user btn-block">--}}
{{--                                Register Account--}}
{{--                            </a>--}}
{{--                            <hr>--}}
{{--                            <a href=" " class="btn btn-google btn-user btn-block">--}}
{{--                                Forgot Password?--}}
{{--                            </a>--}}
{{--                            <a href="{{url('/')}} " class="btn btn-facebook btn-user btn-block">--}}
{{--                                Already have an account? Login--}}
{{--                            </a>--}}
{{--                        </form>--}}
{{--                        <hr>--}}
{{--                        <div class="text-center">--}}
{{--                            This is a common platform of SUSTIANs,--}}
{{--                            where people from SUST of different professions can engage to each other.--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

{{--<footer class="footer footer-alt footer">--}}
{{--    <span class="text-white" target="blank">Developed by</span> <span style="text-decoration: underline;"><a href="https://ogroni.com/" class="text-white" target="blank">Ogroni Informatix Limited</a></span>--}}
{{--</footer>--}}

{{--<!-- Bootstrap core JavaScript-->--}}
{{--<script src="{{asset('ui/backend/')}}/vendor/jquery/jquery.min.js"></script>--}}
{{--<script src="{{asset('ui/backend/')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>--}}

{{--<!-- Core plugin JavaScript-->--}}
{{--<script src="{{asset('ui/backend/')}}/vendor/jquery-easing/jquery.easing.min.js"></script>--}}

{{--<!-- Custom scripts for all pages-->--}}
{{--<script src="{{asset('ui/backend/')}}/js/sb-admin-2.min.js"></script>--}}

{{--</body>--}}

{{--</html>--}}


