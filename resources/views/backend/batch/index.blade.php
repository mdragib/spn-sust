@extends('backend.layouts.master')

@section('title', 'Batch')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Batch</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('batch.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered"  width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Session</th>
                            <th>Department</th>
                            <th>Batch</th>

                            <th style="width: 150px; text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(session()->has('status'))
                            <div class="alert alert-success">

                                <p>{{session('status')}}</p>

                            </div>
                        @endif

                        @foreach($batches as $batch)
                            <tr>
                                <td>{{++$sl}}</td>

                                <td>{{$batch->session->session}}</td>
                                <td>{{$batch->department->name}}</td>
                                <td>{{$batch->batch_number}}</td>


                                <td>
{{--                                    <a href="{{ route('country.show', $country->id) }}" class="btn btn-sm btn-outline-info">Show</a>--}}
                                    <a href="{{ route('batch.edit', $batch->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>
                                    {{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}
{{--                                    <form action="{{route('country.destroy', $country->id)}}" method="post">--}}
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Are you sure Want to delete?')">Delete</button>--}}

{{--                                    </form>--}}


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $batches->links() }}
            </div>

        </div>

    </div>
@endsection

@push('css')
    <!-- Custom styles for this page -->
    <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('script')
    <!-- Page level plugins -->
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
@endpush

