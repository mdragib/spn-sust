
        <div class="form-row">
            <div class="col-md-12 mb-5">

                {{Form::label('session_id','Session')}}<br>
                {{Form::select('session_id',$sessions, null,
                       ['class'=>'form-control',
                       'id'=>'session_id',
                       ])}}

            </div>
        </div>

        <div class="form-row">
            <div class="col-md-12 mb-5">

                {{Form::label('department_id','Department')}}<br>
                {{Form::select('department_id',$departments, null,
                       ['class'=>'form-control',
                       'id'=>'department_id',
                       ])}}

            </div>
        </div>

       <div class="form-row form-group">
            <div class="col-md-12 mb-3 ">
                {{Form::label('batch_number','Batch Number')}}
                {{Form::text('batch_number', null, [
                      'class'=> $errors->has('batch_number') ?  'form-control is-invalid': 'form-control',
                      'placeholder'=>'Enter The batch number',
                      'id'=>'name',
                ])}}
                @error('name')
                <div class="text-danger">{{ $message }}</div>
                @enderror

            </div>
        </div>


