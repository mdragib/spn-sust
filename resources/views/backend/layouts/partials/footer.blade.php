<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class=" text-center my-auto">
            <span class="text-blue" target="blank">Developed by</span> <span  style="text-decoration: underline;"><a href="https://ogroni.com/" class="text-black" target="blank">Ogroni Informatix Limited</a></span>
        </div>
    </div>
</footer>

