<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="https://sustian.xyz">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"> SUSTIAN Professional Network</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <a class="nav-link" href="{{url('/dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/news-and-events')}}">
            <i class="fa fa-book"></i>
            <span>News and Events</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/about-us')}}">
            <i class="fa fa-book" aria-hidden="true"></i>
            <span>About Us</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/privacy-policy')}}">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <span>Privacy Policy</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/contact-us')}}">
            <i class="fa fa-phone-square" aria-hidden="true"></i>
            <span>Contact Us</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/blog')}}">
            <i class="fas fa-blog"></i>
            <span>Blog</span></a>
    </li>
    @if (Auth::user()->role == 'ADMIN' || Auth::user()->role == 'MODERATOR')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
               aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-fw fa-folder"></i>
                <span>Admin Setup</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('country.index')}}">Country</a>
                    <a class="collapse-item" href="{{route('state.index')}}">State</a>
                    <a class="collapse-item" href="{{route('city.index')}}">City</a>
                    <a class="collapse-item" href="{{route('department.index')}}">Department</a>
                    <a class="collapse-item" href="{{route('session.index')}}">Session</a>
                    <a class="collapse-item" href="{{route('occupation.index')}}">Occupation</a>
                    <a class="collapse-item" href="{{url('/pending/users')}}">Pending Users</a>
                    <a class="collapse-item" href="{{route('pending_emails')}}">Pending Emails</a>
                    <a class="collapse-item" href="{{route('active_users')}}">Active Users</a>
                    <a class="collapse-item" href="{{route('deactivate_users')}}">Deactivate Users</a>



                </div>
            </div>
        </li>
    @endif

    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
