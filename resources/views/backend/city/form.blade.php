{{--<ul class="bg-warning text-white">--}}
{{--    @foreach ($errors->all() as $error)--}}
{{--        <li>{{ $error }}</li>--}}
{{--    @endforeach--}}
{{--</ul>--}}

<div class="form-group">
    {!! Form::label('countrySelect', 'Country'); !!}
    {!! Form::select('country_id', $countries, null, [
                                                               'class'=>'form-control',
                                                               'id'=>'countrySelect',
                                                               'required',
                                                               'placeholder'=>'select one',
                                                           ]) !!}
</div>

<div class="form-group">
    {!! Form::label('stateSelect', 'State'); !!}
    {!! Form::select('state_id', $states, null, [
                                                               'class'=>'form-control',
                                                               'id'=>'stateSelect',
                                                               'required',
                                                           ]) !!}
</div>

<div class="form-row form-group">
    <div class="col-md-12 mb-3 ">
        {{Form::label('name','City')}}
        {{Form::text('name', null, [
              'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control','form-control-user',
              'placeholder'=>'Enter The city name',
              'id'=>'name',
        ])}}
        @error('name')
        <div class="text-danger">{{ $message }}</div>
        @enderror

    </div>
</div>


@section('inline-script')
    <script>
        function setStates() {
            var country = $("#countrySelect").val();
            $("#stateSelect").html("")

            var option = "";
            $.get('/get-states/' + country,
                function (data) {
                    data = JSON.parse(data)
                    option += "<option value=' '>Select State</option>";

                    data.forEach(function (element) {
                        option += "<option value='" + element.id + "'>" + element.name + "</option>";
                    });
                    $("#stateSelect").html(option)

                })
        }
        $("#countrySelect").change(function () {
            setStates();
        });


    </script>
@endsection


