        <div class="form-row">
            <div class="col-md-12 mb-5">

                {{Form::label('country_id','Country')}}<br>
                {{Form::select('country_id',$countries, null,
                       ['class'=>'form-control',
                       'id'=>'country_id',
                       ])}}

            </div>
        </div>

       <div class="form-row form-group">
            <div class="col-md-12 mb-3 ">
                {{Form::label('name','State')}}
                {{Form::text('name', null, [
                      'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control','form-control-user',
                      'placeholder'=>'Enter The State name',
                      'id'=>'name',
                ])}}
                @error('name')
                <div class="text-danger">{{ $message }}</div>
                @enderror

            </div>
        </div>


