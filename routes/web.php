<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::group(['middleware' => ['auth', 'verified', 'approved_user']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::post('/', 'HomeController@postDashboard');
    Route::post('/dashboard', 'HomeController@postDashboard');
    Route::get('/member/{id}', 'HomeController@profile');
});


Route::group(['middleware' => ['auth', 'verified', 'approved_user', 'authorized:ADMIN,MODERATOR']], function () {
    Route::resource('country', 'CountryController');
    Route::resource('state', 'StateController');
    Route::resource('city', 'CityController');
    Route::resource('department', 'DepartmentController');
    Route::resource('session', 'SessionController');
    Route::resource('batch', 'BatchController');
    Route::get('/pending/users', 'HomeController@getPendingUser');
    Route::post('/registration/approve', 'HomeController@approveUser');
    Route::post('/registration/decline', 'HomeController@declineUser');
    Route::resource('occupation', 'OccupationController');

    Route::get('/pending/emails', 'UserController@getPendingEmail')->name('pending_emails');
    Route::post('/email/verify', 'UserController@approveEmail')->name('email_verify');
    Route::get('/active/users', 'UserController@activeUser')->name('active_users');
    Route::post('/account/deactivate', 'UserController@deactivateAccount')->name('account_deactivate');
    Route::get('/deactivate/users', 'UserController@deactivateUser')->name('deactivate_users');
    Route::post('/account/activate', 'UserController@activateAccount')->name('account_activate');






});


Route::group(['middleware' => ['auth', 'verified', 'authorized:ADMIN']], function () {
    Route::get('/export/{data}', 'HomeController@export');
});


Route::get('get-cities/{id}', function ($id) {
    return json_encode(\App\City::where('state_id', $id)->get());
});
Route::get('get-department-short-codes/{id}', function ($id) {
    return json_encode(\App\Department::where('id', $id)->get());
});
Route::get('get-batches/{id}', function ($id) {
    return json_encode(\App\Session::where('id', $id)->get());
});
Route::get('get-states/{id}', function ($id) {
    return json_encode(\App\State::where('country_id', $id)->get());
});


Route::get('/about-us', function () {
    return view('frontend.about-us.about-us');
});
Route::get('/blog', function () {
    return view('frontend.blog.blog');
});
Route::get('/contact-us', function () {
    return view('frontend.contact-us.contact-us');
});
Route::get('/news-and-events', function () {
    return view('frontend.news-and-events.news-and-events');
});
Route::get('/privacy-policy', function () {
    return view('frontend.privacy-policy.privacy-policy');
});






